#! /usr/bin/env python

'''
   Setup script for mandaye_cud RP
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

import mandaye_cud

install_requires=[
        'gunicorn>=0.17',
        'mandaye>=0.8.0',
        'whitenoise>=1.0'
]

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    return mandaye_cud.__version__

setup(name="mandaye_cud",
      version=get_version(),
      license="AGPLv3 or later",
      description="mandaye_cud rp is a Mandaye project, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Jerome Schneider",
      author_email="jschneider@entrouvert.com",
      maintainer="Jerome Schneider",
      maintainer_email="jschneider@entrouvert.com",
      scripts=['manager.py', 'server.py'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires
)

