function mandaye_disassociate_logout(url, account, id)
{
  var r = confirm("Etes-vous sûr de vouloir délier le compte " + account + " ?");
  if (r == true)
  {
    window.location.href = url + "?id=" + id;
  }
}

(function( $ ) {
  $(function() {
    $.get("/studio/mandaye/toolbar", function(data) {
      $("body").prepend(data);
    });
    $("#logout_link").off('click').on("click", function() {
      window.location = "/studio/locallogout?next_url=/studio/accueil";
    });
  });
})(mjQuery);

