
"""
You need to defined 3 variables :

* form_values (defined the login form values):
form_values = {
    'login_url': '/login',
    'post_url': '/login',
    'form_attrs': { 'name': 'form40', },
    'username_field': 'user',
    'password_field': 'pass',
    'post_fields': ['birthdate', 'card_number']
}
login_url, form_attrs, post_fields and username_field are obligatory
* urls (a dictionnary with urls) :
 urls = {
       'associate_url': '/mandaye/associate',
       'connection_url': '/mandaye/sso',
       'login_url': '/mandaye/login'
       }
* mapping
"""

from mandaye.auth.saml2 import END_POINTS_PATH

from mandaye_cud.filters.cud import ArcopoleFilter
from mandaye.filters.default import MandayeFilter

form_values = {
        'login_url': '/studio/accueil?callback=acallback',
        'post_url': '/studio/login?callback=acallback',
        'form_attrs': { 'id': 'login-form' },
        'post_fields': ['username', 'password'],
        'username_field': 'username',
        'password_field': 'password',
        }

urls = {
        'associate_url': '/studio/mandayeassociate',
        'connection_url': '/mandaye/sso',
        'disassociate_url': '/mandaye/disassociate',
        'login_url': '/mandaye/login'
        }

is_user_locally_logged_in = ArcopoleFilter.is_user_locally_logged_in

mapping = [
        {
            'path': r'/',
            'on_response': [{
                'filter': MandayeFilter.fix_response_abs_url,
                'content-types': ['application/x-javascript', 'text/html', 'application/xml'],
                }]
            },
        {
            'path': r'^/$',
            'method': 'GET',
            'redirect': '/studio/accueil'
            },
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                'values': {'condition': "'success' in response.msg"},
                },
            },  
        {   
            'path': r'/mandaye/sso$',
            'method': 'GET',
            'response': {'auth': 'sso',}
            },  
        {
            'path': r'/studio/locallogout$',
            'method': 'GET',
            'response': {'filter': ArcopoleFilter.local_logout, 'values': {'next_url': '/studio/accueil'}}
            },
        {   
            'path': r'/mandaye/slo$',
            'method': 'GET',
            'response': {'auth': 'slo',}
            },
        {
            'path': r'/mandaye/disassociate$',
            'method': 'GET',
            'response': {
                'auth': 'disassociate',
                'values': {'next_url': '/mandaye/logout',}
                }
            },

        {
            'path': r'/studio/mandayeassociate$',
            'method': 'GET',
            'target': '/studio/accueil',
            'on_request': [
                {'filter': ArcopoleFilter.associate_req,}
                ],
            'on_response': [{
                'filter': ArcopoleFilter.associate,
                'values': {
                    'action': urls['associate_url'],
                    'template': 'arcopole/associate.html',
                    },

                },]
            },
        {
                'path':  r'/studio/mandayeassociate$',
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    'values': {
                        'condition': "'success' in response.msg",
                        }
                    },
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {
                    'auth': 'single_sign_on_post',
                    'values': {
                        'login_url': urls['login_url'],
                        'next_url': '/studio/accueil'
                        }
                    }
                },
        {
                'path': r'/studio/mandaye_singlelogout$',
                'method': 'GET',
                'response': {
                    'auth': 'single_logout',
                    }
                },
        {
                'path': r'/studio/mandaye_singlelogoutreturn$',
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': '/studio/locallogout',
                        }
                    }
                },
        {
                'path': r'/studio/mandaye/toolbar',
                'response': {'filter': MandayeFilter.toolbar}
                },
        ]
