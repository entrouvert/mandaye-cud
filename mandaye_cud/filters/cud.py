
import json
from urlparse import parse_qs

from mandaye.http import HTTPRequest, HTTPHeader
from mandaye.server import get_response
from mandaye.template import serve_template
from mandaye.response import _302

class ArcopoleFilter:

    @staticmethod
    def associate_req(env, values, request):
        request.cookies = None
        return request

    @staticmethod
    def associate(env, values, request, response):
        qs = parse_qs(env['QUERY_STRING'])
        if qs.has_key('type'):
            values['type'] = qs['type'][0]
        else:
            values['type'] = None
        associate = serve_template(values.get('template'), **values)
        response.msg = associate
        return response

    @staticmethod
    def is_action_logout(env, request, response):
        request.msg = request.msg.read()
        if "logout" in request.msg:
            return True
        return False

    @staticmethod
    def local_logout(env, values, request, response):
        env['beaker.session'].delete()
        req_cookies = request.cookies
        for cookie in req_cookies.values():
            cookie['expires'] = 'Thu, 01 Jan 1970 00:00:01 GMT'
            cookie['path'] = '/studio'
        return _302(values['next_url'], req_cookies)

    @staticmethod
    def is_user_locally_logged_in(env, request, response):
        if "/studio/clients" in env['HTTP_REFERER']:
            return True
        request = HTTPRequest(request.cookies, HTTPHeader())
        response = get_response(env, request,
                "/studio/orion/userinfo?token=__session_token")
        if response.code == 500:
            return False
        auth = json.loads(response.msg)
        if auth['uid'] == "anonymous":
            return False
        return True
